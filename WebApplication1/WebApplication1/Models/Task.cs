namespace WebApplication1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Task")]
    public partial class Task
    {
        public int TaskID { get; set; }

        public int? TaskPriority { get; set; }

        [Required]
        [StringLength(200)]
        public string Content { get; set; }

        public DateTime AddTime { get; set; }

        public int? TaskState { get; set; }

        public DateTime? FinishedTime { get; set; }

        public int? uid { get; set; }

        public virtual userinfo userinfo { get; set; }
    }
}
