﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class TeskController : Controller
    {
        // GET: Tesk
        public ActionResult Index()
        {
            //ViewData["Id"] = 1001;
            //TempData["Name"] = "张三";
            //ViewBag.Age = 18;

            Model1 model = new Model1();

            var task = model.Task.ToList();
            return View(task);
            ViewData["task"] = task;
            /*
             View()向后台传值
            1.后台传值 View（数据）
            2.前台取值
                2.1 类型约束    @model  目标类型=>与数据类型一致或可进行自动类型转换
                2.2 取值 Model=>目标类型
             */
            return View();
        }
        public ActionResult Detall()
        {
            return View();
        }
    }
}