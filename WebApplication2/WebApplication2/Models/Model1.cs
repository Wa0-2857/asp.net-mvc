namespace WebApplication2.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=Model1")
        {
        }

        public virtual DbSet<Task> Task { get; set; }
        public virtual DbSet<userinfo> userinfo { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Task>()
                .Property(e => e.Content)
                .IsUnicode(false);

            modelBuilder.Entity<userinfo>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<userinfo>()
                .Property(e => e.pwd)
                .IsUnicode(false);
        }
    }
}
