﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;
namespace WebApplication2.Controllers
{
    public class TaskController : Controller
    {
        // GET: Task
        public ActionResult Index()
        {
            /* 
             后台向前台传值
             */
            //ViewData["Id"] = 1001;
            //TempData["Name"] = "张三";
            //ViewBag.Age = 18;

            Model1 model = new Model1();
            //var task= model.Task.FirstOrDefault(p => p.TaskID == 1);
            //ViewData["task"] = task;
            //ViewBag.Task = task;

            /*
             View()向前台传值
                1.后台传值       View(数据)
                2.前台取值
                    2.1 类型约束   @model 目标类型=> 与数据类型一致或可进行自动类型转换
                    2.2取值 Model => 目标类型
             */
            var task = model.Task.ToList();
            return View(task);
        }
        //详情
        public ActionResult Detail(Task t)
        {
            //var id = Request.QueryString["TaskID"];
            Model1 model = new Model1();
            var task = model.Task.FirstOrDefault(p => p.TaskID == t.TaskID);
            //ViewData["task"] = task;
            //ViewBag.Task = task;
            return View(task);
        }
        //删除
        [HttpPost]//只能处理Post请求
        public ActionResult Delete(int TaskID)
        {
            Model1 model = new Model1();
            var task = model.Task.FirstOrDefault(p => p.TaskID == TaskID);
            if(task != null)
            {
                model.Task.Remove(task);
                var result = model.SaveChanges()>0;
                return Json(result);
            }
            return Json(false);
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Task task)
        {
            task.TaskPriority = 1;
            task.TaskState = 1;
            task.uid = 1;
            Model1 model = new Model1();
            model.Task.Add(task);
            var result = model.SaveChanges();
            if (result > 0)
            
                return RedirectToAction("Index");
               return View();
            
        }
    }
}