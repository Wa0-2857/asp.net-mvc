use master  --为了删除
go

if exists (select * from sys.databases where name='Test')
	drop database Test
go

create database Test  --创建数据库
go

use Test	--使用数据库
go

create table userinfo
(
	uid int primary key identity(1,1),
	name varchar(10) not null,
	pwd varchar(10) not null,
	role int default(1)
)

create table Task
(
	TaskID int primary key identity(1,1),
	TaskPriority int check(TaskPriority=1 or TaskPriority=2 or TaskPriority=3),
	Content varchar(200) not null,
	AddTime datetime default(getdate()) not null,
	TaskState int default(1) check(TaskState=1 or TaskState=2),
	FinishedTime datetime default(null),
	uid int references userinfo(uid)
)
go

insert into userinfo values('张三','123456',1)
insert into userinfo values('李四','123456',2)

insert into Task values(1,'进行数据库设计','2016-05-11',2,'2016-05-14',1);
insert into Task values(2,'实现审批申请单','2016-05-11',1,null,2);
insert into Task values(3,'测试整个系统','2016-05-11',1,null,2);
insert into Task values(2,'实现登陆','2016-05-11',2,'2016-05-14',1);
